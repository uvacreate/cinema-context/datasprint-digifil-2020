# Work division

Below you'll find a division of the workload. Each participant (indicated with their initials) was assigned 3 weeks per year. 
Please stick to the weeks that were assigned to you, to prevent confusion. You are free to choose in which order you want to work through the weeks.
In the happy event that you finish all the weeks before the end of the datasprint, let us know; we would feel obliged to send you flowers or wine ;)


| initials | weeks 1952 | weeks 1962 | weeks 1972 |
| -------- | ---------- | ---------- | ---------- |
| CPO      | 1 to 3     | 1 to 3     | 1 to 3     |
| DTG      | 4 to 6     | 4 to 6     | 4 to 6     |
| ER       | 7 to 9     | 7 to 9     | 7 to 9     |
| GE       | 10 to 12   | 10 to 12   | 10 to 12   |
| HK       | 13 to 15   | 13 to 15   | 13 to 15   |
| HW       | 16 to 18   | 16 to 18   | 16 to 18   |
| JN       | 19 to 21   | 19 to 21   | 19 to 21   |
| KL       | 22 to 24   | 22 to 24   | 22 to 24   |
| LM       | 25 to 27   | 25 to 27   | 25 to 27   |
| LVV      | 28 to 30   | 28 to 30   | 28 to 30   |
| NN       | 31 to 33   | 31 to 33   | 31 to 33   |
| PE       | 34 to 36   | 34 to 36   | 34 to 36   |
| TO       | 37 to 39   | 37 to 39   | 37 to 39   |
| WH       | 40 to 42   | 40 to 42   | 40 to 42   |
| MdE      | 43 to 45   | 43 to 45   | 43 to 45   |
| RJ       | 46 to 48   | 46 to 48   | 46 to 48   |
| X2       | 49 to 52   | 49 to 52   | 49 to 52   |