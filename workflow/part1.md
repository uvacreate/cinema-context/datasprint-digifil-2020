# Workflow part 1: Finding correct titles


## Some general comments

We limit ourselves to feature films, and ignore newsreels (often described as ‘nieuws’ or ‘actualiteiten’) and short film programmes, cartoons, etcetera. So, usually the “Cineac_regbr” field (Cineac Reguliersbreestraat) will remain empty, because this was a newsreel theatre.

For the 1960s, and even more so for the 1970s, the data quality is less than for the 1950s. This is because we have a good list of (Dutch) film titles until 1960. For the period after that, we still lack an authoritative list of Dutch title variants.


## 1a) Finding the correct titles

At the start of the datasprint, each participant will be allocated a number of weeks. It is important to keep to the allocated weeks, to prevent that several participants work on the same list.

Please enter your name in the designated field on top of the page. This will allow us to keep track of the data provenance. Then retrieve the data for the desired week by pressing ‘list’.

Choose a newspaper film listing either in OCRed version or the scan from the yellow section. Use one (or if needed, several) newspaper film listings to check the pre-filled list in the green section. Sometimes, one newspaper listing is more complete than the other, containing a larger selection of cinemas.

If the pre-filled title is correct, no further action is required. If there is no title entered in the list, this can be correct (if no film was screened this week), then again no further action is required. 

If a wrong title is filled out in the list, or incorrectly no title was entered, please fill in the correct title, spelled as in the original source. 


## 1b) Extra screenings

In the ‘60s and ‘70s, for many Amsterdam cinemas special screenings were listed; besides the regular film, some cinemas offered children’s matinees (usually 1-3 screenings per week, usually on Wednesday, Saturday and/or Sunday afternoons), and others offered special night screenings that started around midnight, usually on Friday and/or Saturday nights. Incidental film screenings, such as film festivals or retrospectives, can be categorised as ‘other’. Extra screenings can be added using the “+” button, and setting the “Type” field to the correct value. If a cinema has only one screening but multiple rows have been created for it, those extra rows can be ignored.
