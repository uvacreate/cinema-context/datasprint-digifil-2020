# Workflow part 2: Identifying the titles

The next phase is the identification of titles. This is the most tricky part. 

For all the titles pre-filled in the green section, there is an automatic match made by the system to a film title either from the IMDb or the Cinema Context database. In the pink section below, you can trace to which original text part the match was made. This can sometimes help you to judge the reliability of the match (often knowledge of Dutch will be required to make this call). A low match ratio and/or a very early year of production can be signs of an incorrect match, hence a wrong identification.

There are three options:

1. you accept the match as (probably) correct (no further action is  required)
2. you replace the match with the correct ID
3. you do not trust the match but have no better alternative (make/leave the field empty)

For ID, we simply use the web address of the IMDb title, for example: [`https://www.imdb.com/title/tt0042340/reference`](https://www.imdb.com/title/tt0042340/reference) to indicate the title “The Company She Keeps (1951)”. 

For a Cinema Context title, please copy and paste the so-called “permalink (see Fig 6): [`http://www.cinemacontext.nl/id/F047500`](http://www.cinemacontext.nl/id/F047500):

![InstructionCC](./images/instructionsCC.jpg)
_Figure 6_

If you want to identify a (Dutch) title, you can start your search in [IMDb](https://www.imdb.com/). IMDb does contain some Dutch titles but not all. Sometimes alternative titles in other languages can give a clue. If you scroll down on an IMDb title page, under ‘Additional details’ you can find a list of alternative titles ‘Also Known As’ (click on ‘See more’) (see Fig 7).
You can look up titles in [Cinema Context](http://www.cinemacontext.nl/), but note that there are no titles of films made after 1960, and that the search interface is outdated (better search parts of the title instead of full title and leave out particles like 'de' or 'het'). 

![Delpher selection](./images/imdbdetails.jpg)
_Figure 7_

But since the Digifil system has already failed to match the title to these databases, you probably need to look further to find out what the original title of the film in question was. If you find some context information, such as the name of a star, director or studio, you will have a better chance of identifying the title. For that context, you can return to the original source: the newspaper.


## Using Delpher to find the original film title

Delpher is the Dutch repository of digitised newspapers used in the Digifil project: [https://www.delpher.nl/nl/kranten](https://www.delpher.nl/nl/kranten)

 If you click on  ‘uitgebreid zoeken’ you will find the advanced search screen (Fig 8):

![Delpher selection](./images/delpheroverview.jpg)
_Figure 8_

Here, you can enter the Dutch film title (optionally include terms like ‘film’ or ‘bioscoop’), the period (not later than the screening week in question) and as ‘type of report’ (‘soort bericht’) enter ‘advert’ (‘advertentie’) (Fig 9). If you find some ads for the film screening, you might either find a mention of the original title, or a mention of the main stars, which can set you on the right path. If you cannot find the title, you can still fill in the names of the stars in the ‘Film ID’ field.

![Delpher selection](./images/delphersearch.jpg)
_Figure 9_

If lucky, you find the original title in the ad (fig 10):

![Delpher selection](./images/delpherselection.jpg)
_Figure 10_

If you only find the names of the main stars, you can first try a google (or your favourite search engine) search. If that does not get you results, we made a simple tool ‘Actor cross-ref’ (Fig 11) to look for all the films listed in IMDb with these two stars in them. However, note this is a very basic tool that it will only work if there is only a limited number of films in which the stars cooperated and it takes quite a long time to get results. 

![Editor](./images/crossref.JPG)
_Figure 11_

To use ‘Actor cross-ref’, click on the link on the top of the page of the Digifil editor. First, you need to get the URLs of the IMDb-page of the stars, for instance: [https://www.imdb.com/name/nm0001918/?ref_=fn_nm_nm_1](https://www.imdb.com/name/nm0001918/?ref_=fn_nm_nm_1) (Louis Armstrong). 