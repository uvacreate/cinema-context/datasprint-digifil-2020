# Summary

* [Introduction](README.md)
* [Communication](communication.md)
* [Digifil Editor](editor/editor.md)
* Workflow
  * [Part 1: Finding correct titles](workflow/part1.md)
  * [Part 2: Identifying the titles](workflow/part2.md)
  * [Work division](workflow/division.md)
* [Results](results/results.md)