# Digifil Editor

In the datasprint we will use the ‘Digifil editor’ for processing of the data, a tool devised by Ivan Kisjes especially for this purpose. It can be found here:
* [http://145.100.59.185/digifil_editor/](http://145.100.59.185/digifil_editor/)



## Introduction

The data is ordered per week, as in the Netherlands film programmes changed weekly. On the top of the page, you can select a year and a week (see Fig 1). By pressing the ‘list’ button, the system retrieves the data for that week. The screen consists of three main sections: The green section contains a selection from the Digifil data that we expect to be the closest to the correct film programme for that week. The pink section below the green one shows all the results from the Digifil data for this particular week, on which the data in the green section is based. And thirdly, there is a yellow section that lists newspaper sources on the days of the week in question, with links to the OCR and the original scan in Delpher (opens in a separate window). Finally, a purple section displays, once the link is clicked, the OCRed text from a selected newspaper for that day. Below a more detailed description.


![Editor](./images/de1.JPG)

_Figure 1_ 

-----

### 1. Yellow/purple sections: Newspaper sources as OCR and scans

The yellow section at the top left contains a list of the film listings that Digifil found for that specific week (see Fig 2). If you click on ‘view OCR’, the OCRed text of an article from that date will appear in the window. If you click on ‘view scan’, the scanned article of that date will appear in a new window. You can choose which option works best for you, and also (if there is more than one), which of the articles/film listings is most complete and/or best legible. Ideally, you can use both in juxtaposition.

 ![Editor](./images/de4.JPG)

_Figure 2_

-----

### 2. The green section: selecting the correct data

 ![Editor](./images/de5.JPG)

_Figure 3_

The green section (Fig 3) contains a selection from the Digifil data that we expect to be the closest to the correct film programme for that week. This is the section where you can actually contribute your work. The section consists of a list of all the cinemas that were active in that specific year and for which you would normally expect a film listing. For each cinema, we have automatically pre-filled this list with our best guess of which film was playing that week, based on the Digifil data as listed in the pink section (see below), or left it empty if we have no data for that cinema in that week. The main task for this datasprint is to check this list and correct or add where necessary. The workflow is described in more detail below. The section also contains a field for comments and a button to confirm the data, saving and sending the form (Fig 4). Note that once confirmed, **you can no longer access the data.**

 ![Editor](./images/de6.JPG)

_Figure 4_

------

_Table 1: Field names green section_

| Field name | Description                                                                                                                                                                                                                       |
| ---------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Cinema     | Standardised cinema name (based on a list of cinemas active in Amsterdam in that particular year)                                                                                                                                 |
| Title      | Film title (as mentioned in either IMDb or Cinema Context)                                                                                                                                                                        |
| Film ID    | Film ID from IMDb (or Cinema Context)                                                                                                                                                                                             |
| Link       | Link to the IMDb or Cinema Context page for that title (in new window)                                                                                                                                                            |
| Year       | Year of production (according to IMDb or Cinema Context)                                                                                                                                                                          |
| Type       | Default this is set to a standard screening (‘standaard’). Can be changed to indicate special screening types: night screenings (‘nacht’) and children’s matinees (‘matinee’) or ‘other’ in special cases such as film festivals. |
| +          | This button creates a new screening, if a cinema offered more than one screening in that particular week.                                                                                                                         |




### 3. The pink section: the full Digifil data

![Editor](./images/de7.JPG)
_Figure 5_

This section shows all the results from the Digifil data for this particular week. Listed here is raw data that contains OCR errors, and can contain entities that are incorrectly identified as cinemas. We have listed this data because it will allow you to see and understand where the data in the green section stems from. Also, it shows additional information on the identification of the title, that was automatically matched to a title stored in IMDb or Cinema Context.

Because we used up to ten newspapers, and each newspaper could print several listings per week, there can be, and often is, more than one row listed per cinema. If more than one title is assigned to a cinema, this can be an error, where only one title is the correct one, or more than one screening took place that week. Also, for various reasons, sometimes _no_ information is available for a certain cinema in a certain week. The algorithm might have missed the listing for that week, _or_ there simply were no screenings that week. In short: in these cases we need human eyes to decide what is the correct information.

_Table 2: Field names pink section_

| Field name | Description                                                                                                                                                                                                                                                       |
| ---------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| datum      | Date of the source, with a link to the original newspaper scan (opens in new window)                                                                                                                                                                              |
| cinema     | Name of the cinema, as recognised by Digifil (includes mistakes and OCR errors)                                                                                                                                                                                   |
| title      | Title of the film, as recognised by Digifil (includes mistakes and OCR errors)                                                                                                                                                                                    |
| article_id | Article ID of the source. Clicking on this will bring up the OCR of the article in question in the window next to the green section.                                                                                                                              |
| ratio      | Match ratio: value between 1 and 100 that denotes the degree of similarity between the OCR’d title from the source and the normalised title from IMDb or Cinema Context to which it was matched. So a 100 score means a complete match and 0 means no similarity. |
| Match      | This is the title from IMDb or Cinema Context (indicated with ‘CC’) that the source title was matched to, plus the year of production. Clicking on the link takes you to the page on either IMDb or CC.                                                           |
| +          | This button creates a new screening, if a cinema offered more than one screening in that particular week.                                                                                                                                                         |
