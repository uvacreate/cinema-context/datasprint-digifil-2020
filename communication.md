# Communication


## Video-conference

Video-conference via [Zoom](https://zoom.us/). Please stay muted unless you want to speak. If you want feedback on a question, we can create separate ‘break-out rooms’ for smaller group discussions. If you have registered for the datasprint, you can find the Zoom meeting id in your mailbox. 


## Chat

For communication outside of Zoom (especially useful if we are separated into ‘break-out rooms’ in Zoom, we can chat using Slack: 

* [Click here to join the CC Datasprint on Slack](https://join.slack.com/t/ccdatasprint/shared_invite/zt-ex0fzzmo-DH_jIr7WGi6Z3RIDUM5olw)

## Other issues?

If you have any problems, you can notify our team members: 
* Thunnis van Oort ([mail](mailto:t.vanoort@uva.nl))
* Ivan Kisjes ([mail](mailto:i.kisjes@uva.nl))
* Leon van Wissen ([mail](mailto:l.vanwissen@uva.nl)).